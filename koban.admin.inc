<?php

/**
 * @file
 * Administrative page callbacks for the koban module.
 */

/**
 * Implements hook_admin_settings() for module settings configuration.
 */
function koban_admin_settings_form($form_state) {

  $koban_logo = '/' . drupal_get_path('module', 'koban') . '/images/logo-koban.png';

  $form['account'] = array(
    '#type' => 'fieldset',
    '#title' => t('Koban Tracking settings'),
  );
  $form['account']['koban'] = [
    '#markup' => '<a href="http://koban.cloud/" target="_blank"><img src="' . $koban_logo . '" alt="Koban CRM" /></a>',
  ];

  $form['account']['koban_apikey'] = array(
    '#title' => t('Koban API Key'),
    '#type' => 'textfield',
    '#default_value' => variable_get('koban_apikey', ''),
    '#required' => TRUE,
    '#description' => t('Your Koban API Key'),
  );

  $form['account']['koban_tracking_enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enabled the Koban Tracking'),
    '#description' => t('Enabled the Koban Tracking on your site'),
    '#default_value' => variable_get('koban_tracking_enabled', FALSE),
  );

  $form['tracking_title'] = array(
    '#type' => 'item',
    '#title' => t('Tracking Page'),
  );
  $form['tracking'] = array(
    '#type' => 'vertical_tabs',
  );

  $visibility = variable_get('koban_tracked_pages', 0);
  $pages = variable_get('koban_pages_list', KOBAN_PAGES);

  $form['tracking']['page_vis_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Pages'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  if ($visibility == 2) {
    $form['tracking']['page_vis_settings'] = array();
    $form['tracking']['page_vis_settings']['koban_tracked_pages'] = array('#type' => 'value', '#value' => 2);
    $form['tracking']['page_vis_settings']['koban_pages_list'] = array('#type' => 'value', '#value' => $pages);
  }
  else {
    $options = array(
      t('Every page except the listed pages'),
      t('The listed pages only'),
    );

    $title = t('Pages');
    $description = t("Specify pages by using their paths. Enter one path per line. The '*' character is a wildcard. Example paths are %blog for the blog page and %blog-wildcard for every personal blog. %front is the front page.",
      array(
        '%blog' => 'blog',
        '%blog-wildcard' => 'blog/*',
        '%front' => '<front>',
      )
    );

    $form['tracking']['page_vis_settings']['koban_tracked_pages'] = array(
      '#type' => 'radios',
      '#title' => t('Add tracking to specific pages'),
      '#options' => $options,
      '#default_value' => $visibility,
    );
    $form['tracking']['page_vis_settings']['koban_pages_list'] = array(
      '#type' => 'textarea',
      '#title' => $title,
      '#title_display' => 'invisible',
      '#default_value' => $pages,
      '#description' => $description,
      '#rows' => 10,
    );
  }

  return system_settings_form($form);
}
